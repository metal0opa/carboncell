const express = require("express");
const cookies = require("cookie-parser");
const app = express();
const cors = require("cors");
const swaggerUi = require("swagger-ui-express");

const connectDb = require("./config/dbConnect");
const userRoutes = require("./routes/userRoutes");
const { CSS_URL, customCss } = require("./utils/constants");
const swaggerSpecs = require("./config/swaggerConfig");
const dataRetrievalRoutes = require("./routes/dataRetrievalRoutes");
const errorHandler = require("./middlewares/errorHandler");

require("dotenv").config();

const port = process.env.PORT || 3000;

const startServer = async () => {
  if (await connectDb()) {
    try {
      app.listen(port, () => {
        console.log(
          `\nSuccessfully Connected to Database...\nListening to Requests at Port: ${port}\nServer Started...`
        );
      });
    } catch (err) {
      console.log(err);
    }
  } else {
    console.log("Server Error");
  }
};

startServer();

app.use(cookies());
app.use(
  cors({
    origin: true,
    credentials: true,
  })
);
app.use(express.json());
app.use((req, res, next) => {
  console.log("\nNew Request Made :");
  console.log("Host : ", req.hostname);
  console.log("Path : ", req.path);
  console.log("Method : ", req.method);
  if (Object.keys(req.body).length !== 0) {
    console.log("Body: ", req.body);
  }
  if (Object.keys(req.query).length !== 0) {
    console.log("Query: ", req.query);
  }
  if (Object.keys(req.params).length !== 0) {
    console.log("Params: ", req.params);
  }
  next();
});

app.use("/api/users/", userRoutes);
app.use("/api/data/", dataRetrievalRoutes);
app.use(
  "/",
  swaggerUi.serve,
  swaggerUi.setup(swaggerSpecs, {
    customCss: customCss,
    customCssUrl: CSS_URL,
  })
);

app.use(errorHandler);
