const asyncHandler = require("express-async-handler");
const { Web3 } = require("web3");

const { NotFoundError, UserError, ServerError } = require("../utils/errors");
const { DATA_RETREIVAL_URL } = require("../utils/constants");
const SuccessResponse = require("../utils/successResponses");
const successHandler = require("./successController");
const web3 = require("../config/web3Connect");

const fetchPublicAPI = asyncHandler(async (req, res, next) => {
  const { category, limit, offset } = req.query;
  const response = await fetch(DATA_RETREIVAL_URL);
  const data = await response.json();

  if (!data) {
    next(new NotFoundError("Data not found"));
    return;
  }

  let filteredData = data.entries;
  filteredData = Object.values(filteredData);

  if (category) {
    filteredData = filteredData.filter((api) => {
      return api.Category.toLowerCase().includes(category.toLowerCase());
    });
  }

  let offsetValue = offset ? parseInt(offset, 10) : 0;
  let limitValue = limit ? parseInt(limit, 10) : 10;

  if (offsetValue > filteredData.length || offsetValue < 0) {
    next(new UserError("Invalid offset value"));
    return;
  }

  filteredData = filteredData.slice(offsetValue);

  if (limitValue > filteredData.length || limitValue < 0) {
    next(new UserError("Invalid limit value"));
    return;
  }

  filteredData = filteredData.slice(0, limitValue);

  successHandler(
    new SuccessResponse("Public APIs fetched successfully", 200),
    res,
    { data: filteredData }
  );
});

const fetchEthereumBalance = asyncHandler(async (req, res, next) => {
  const { address } = req.query;

  if (!address) {
    next(new UserError("Address is required"));
    return;
  }

  if (!Web3.utils.isAddress(address)) {
    next(new UserError("Invalid Ethereum address"));
    return;
  }
  const balance = await web3.eth.getBalance(address);
  const balanceInEther = web3.utils.fromWei(balance, "ether");

  successHandler(
    new SuccessResponse("Ethereum balance fetched successfully", 200),
    res,
    { balanceInEther }
  );
});

module.exports = { fetchPublicAPI, fetchEthereumBalance };
