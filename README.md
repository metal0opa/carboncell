# Server

## Installation

```bash
npm install
```

## Running the server

```bash
npm start
```

## Endpoints

### POST /api/users/register

```json
{
  "name": "username",
  "email": "email",
  "password": "password"
}
```

### POST /api/users/login

```json
{
  "email": "email",
  "password": "password"
}
```

### GET /api/users/get

### POST /api/users/logout

### POST /api/data/balance?address=<your ethereum address>

### POST /api/data/api?category=<category>&offset=<offset>&limit=<limit>