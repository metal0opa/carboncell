/**
 * @swagger
 * tags:
 *   name: Data Retrieval
 *   description: API endpoints for data retrieval
 */

/**
 * @swagger
 * /api/data/api:
 *   get:
 *     summary: Retrieve public APIs
 *     description: Retrieve a list of public APIs from an external source.
 *     tags: [Data Retrieval]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: query
 *         name: category
 *         schema:
 *           type: string
 *         description: Filter APIs by category
 *       - in: query
 *         name: limit
 *         schema:
 *           type: integer
 *         description: Limit the number of results
 *       - in: query
 *         name: offset
 *         schema:
 *           type: integer
 *         description: Offset for paginated results
 *     responses:
 *       '200':
 *         description: Public APIs retrieved successfully
 *       '401':
 *         description: Unauthorized - Token is missing or invalid
 *       '500':
 *         description: Internal server error
 */

/**
 * @swagger
 * /api/data/balance:
 *   get:
 *     summary: Retrieve Ethereum account balance
 *     description: Retrieve the balance of a specified Ethereum account.
 *     tags: [Data Retrieval]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: query
 *         name: address
 *         schema:
 *           type: string
 *         description: Ethereum wallet address
 *         required: true
 *     responses:
 *       '200':
 *         description: Ethereum balance retrieved successfully
 *       '401':
 *         description: Unauthorized - Token is missing or invalid
 *       '500':
 *         description: Internal server error
 */

const express = require("express");
const router = express.Router();

const dataRetrievalController = require("../controllers/dataRetrievalController");
const { requireAuth } = require("../middlewares/requireAuth");

router.get("/api", requireAuth, dataRetrievalController.fetchPublicAPI);

router.get(
  "/balance",
  requireAuth,
  dataRetrievalController.fetchEthereumBalance
);

module.exports = router;
