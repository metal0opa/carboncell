const { titles } = require("../utils/constants");
require("dotenv").config();

const errorHandler = (error, req, res, next) => {
  const statusCode = error.statusCode ? error.statusCode : 500;

  if (statusCode >= 500) {
    console.error(error.message);
    console.error(error.stack);
  }

  if (statusCode in titles) {
    res.status(statusCode).json({
      title: titles[statusCode],
      name: error.name,
      message: error.message,
      ...(process.env.NODE_ENV === "development"
        ? { stackTrace: error.stack }
        : {}),
    });
  } else {
    if (error) {
      res.status(statusCode).json({
        title: "Unknown error",
        name: error.name,
        message: "Unknown error occurred",
        ...(process.env.NODE_ENV === "development"
          ? { stackTrace: error.stack }
          : {}),
      });
    } else {
      console.error("Unhandled Exception Occurred");
    }
  }
};

module.exports = errorHandler;
