const { Web3 } = require("web3");

const httpProvider = new Web3.providers.HttpProvider(
  process.env.ETHEREUM_RPC_URL
);
const web3 = new Web3(httpProvider);

module.exports = web3;
