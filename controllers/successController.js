const { titles } = require("../utils/constants");
require("dotenv").config();

const successHandler = (obj, res, fields) => {
  const statusCode = obj.statusCode ? obj.statusCode : 200;

  if (statusCode in titles) {
    res.status(statusCode).json({
      title: titles[statusCode],
      message: obj.message,
      ...fields,
    });
  } else {
    res.status(200).json({
      title: "OK",
      message: "",
    });
  }
};

module.exports = successHandler;
