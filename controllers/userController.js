const asyncHandler = require("express-async-handler");
const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");

const { NotFoundError, UserError, ServerError } = require("../utils/errors");
const SuccessResponse = require("../utils/successResponses");
const successHandler = require("./successController");

const { User } = require("../models/userModel");

const dotenv = require("dotenv");
dotenv.config();

// JWT cookie persists for 1 day (value below is in ms)
const maxAge = 1 * 24 * 60 * 60 * 1000;

const registerUser = asyncHandler(async (req, res, next) => {
  const { name, email, password } = req.body;

  if (!name || !email || !password) {
    next(new UserError("All fields are necessary"));
    return;
  }

  try {
    const checkEmail = await User.findOne({ email });
    if (checkEmail) {
      next(new UserError("Email already taken"));
      return;
    }

    const hashedPassword = bcryptjs.hashSync(password, 10);

    const newUser = new User({
      name,
      email,
      password: hashedPassword,
    });

    try {
      await newUser.save();
      const { password, __v, createdAt, updatedAt, _id, ...otherFields } =
        newUser._doc;
      successHandler(
        new SuccessResponse("User created successfully", 201),
        res,
        otherFields
      );
    } catch (err) {
      console.error(err);
      next(new ServerError("User could not be created"));
    }
  } catch (error) {
    console.error(error);
    next(new ServerError("Unknown error"));
  }
});

function createToken(id, age = maxAge) {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: age,
  });
}

const loginUser = asyncHandler(async (req, res, next) => {
  const { email, password } = req.body;

  try {
    const user = await User.login(email, password);
    const token = createToken(user._id);
    res.cookie(
      "jwt",
      token
      // {
      //   httpOnly: true,
      //   maxAge: maxAge,
      //   domain: process.env.DOMAIN,
      //   sameSite: "none",
      //   secure: true,
      // }
    );
    successHandler(new SuccessResponse(`Logged in`, 202), res, {
      id: user._id,
    });
  } catch (err) {
    next(new UserError("Invalid username / password", 403));
  }
});

const logout = asyncHandler(async (req, res) => {
  res.clearCookie("jwt");
  successHandler(new SuccessResponse("Logged Out!"), res);
});

const getUserData = asyncHandler(async (req, res, next) => {
  const token = req.cookies.jwt;
  let id;

  try {
    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
      if (err) {
        next(new UserError("Invalid JWT", 403));
        return;
      } else {
        id = decoded.id;
      }
    });

    const userDoc = await User.findById(id);
    if (userDoc) {
      const { password, __v, createdAt, updatedAt, _id, ...otherFields } =
        userDoc._doc;
      successHandler(new SuccessResponse("User Found"), res, otherFields);
    } else {
      next(new NotFoundError("User not found"));
    }
  } catch (error) {
    console.error(error);
    next(new ServerError("Unknown error"));
  }
});

module.exports = {
  registerUser,
  loginUser,
  logout,
  getUserData,
};
